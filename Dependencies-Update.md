# Dependencies Update
A step by step guide with CLI commands.


### Mandatory
You need to install `hg` (mercurial) and have access rights and third parts commit push rights, by [creating an issue here](https://jira.eyeo.com/projects/HD/issues/HD-379?filter=allissues).

Once you have `hg` installed, optionally enable `purge` extension:

```sh
hg config --edit
```

The file should have your data in it, but also optionally `purge =` at its bottom.
```
[ui]
# name and email, e.g.
# username = Jane Doe <jdoe@example.com>
username = Your Name <your.email@eyeo.com>

...

[extensions]
# uncomment the lines below to enable some popular extensions
# (see 'hg help extensions' for more info)
#
# histedit =
# rebase =
# uncommit =
purge =
```

To clone the ABP UI repository, create a folder (i.e. `~/mercurial/`), go there and:

```sh
hg clone ssh://hg@hg.adblockplus.org/adblockplusui
```


## Step 1: check live documentation

It is currently [here](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/wikis/release-workflow#make-release) and it contains a description of all necessary steps to release.


## Step 2: be sure the release is updated

Checkout the release branch you'd like to land in mercurial. I'll be calling it `release-branch` for simplicity sake, and also for simplicity sake, I'll store that name into a variable.

```sh
# put the real branch name
export RELEASE="release-branch"

# enter the GitLab ABP UI folder
cd adblockplusui

# Checkout and update master
git checkout master
git pull --rebase origin master

# Checkout and update the release
git checkout "${RELEASE}"
git pull --rebase origin "${RELEASE}"

# Optionally merge into the release changes from master
# git pull --rebase origin master
```


## Step 3: Create a patch

```sh
# grab the branch
export BRANCH="$(git rev-parse --abbrev-ref HEAD)"

# verify you are using the right branch
if [ "${BRANCH}" != "${RELEASE}" ]; then
  # get out if something mismatches
  echo "Error: using "${BRANCH}" to release ${RELEASE}"
  exit 1
fi

# create a folder with such branch name
mkdir -p ~/releases/${BRANCH}

# create the patch
git format-patch master --stdout > ~/releases/${BRANCH}/git.patch

# optionally have a look at the patch file, be sure all is fine
# cat ~/releases/${BRANCH}/git.patch
```


## Step 4: Update mercurial repository
```sh
# enter the mercurial version of ABP UI
cd ~/mercurial/adblockplusui

# update, purge, pull and checkout default => master
hg update -C
hg purge
hg pull
hg checkout default
hg bookmark master
```


## Step 5: Import and push

```sh
# import the patch
hg import --exact ~/releases/${BRANCH}/git.patch

# if --exact fails, you can either import with wrong dates
# or you could use this patched import (suggested)
# https://github.com/ThomasGreiner/moz-git-tools/tree/fixed-hg-patch-date

# tag the current release
hg tag "${BRANCH}"

# add the `Noissue - ` prefix to the tag
hg commit --amend

# now you can push all changes to master
hg push -r master
```

## Step 6: Update the dependencies update issue (via milestone)

Example: https://gitlab.com/eyeo/adblockplus/adblockpluschrome/issues/122

Follow last steps from the [documentation](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/wikis/release-workflow#make-release) to complete the dependencies update.

To have the hg tag to copy and paste there:
```
# the TBD hash
echo "hg:$(hg heads | grep 'changeset:' | sed -e 's/changeset:[^:]*://')"
```

To have the git tag to copy and paste there, go back in your GitLab ABP UI folder, and then:
```
git checkout master
echo "git:$(git rev-parse HEAD)"
```
