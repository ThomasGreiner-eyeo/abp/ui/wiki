# Definition of Done

An issue is considered done - as in that the corresponding change is ready to be merged into the release branch - if **all** of the following questions can be answered with _Yes_.

## Meta

- Has milestone?
- Has label ~"State::Implementation Review (ABPUI)"?
- Does not have label ~"Blocked (ABPUI)"?
- Is added to corresponding Updates Page issue if applicable

## Description

- If has label ~"Bug (ABPUI)":
  - Has `Environment` section?
    - Differs from template?
    - Is not `TBD`?
  - Has `Steps to reproduce` section?
    - Differs from template?
    - Is not `TBD`?
  - Has `Observed behavior` section?
    - Differs from template?
    - Is not `TBD`?
  - Has `Expected behavior` section?
    - Differs from template?
    - Is not `TBD`?
- else:
  - Has `What to change` section?
    - Has `Design` point?
      - Has link or is `N/A`?
    - Has `Research` point?
      - Has link or is `N/A`?
    - Has `Spec` point?
      - Has link or is `N/A`?
    - Has `Development` point?
      - Has list items or is `N/A`?
    - Either `Spec` or `Development` point is not `N/A`?
  - Has `Hints for testers` section?
    - Differs from template?
    - Is not `TBD`?
  - Has `Hints for translators` section?
    - Differs from template?
    - Is not `TBD`?
  - Has `Integration notes` section?
    - Differs from template?
    - Is not `TBD`?

## Related Merge Requests

- Links to adblockplusui merge request whose title references corresponding issue number?
- If has `Spec` point:
  - Links to spec merge request whose title references corresponding issue number?
  - Spec merge request has been merged into corresponding spec release branch?
- If has `Integration notes` section:
  - Links to adblockpluschrome and/or adblockpluscore merge request whose title references corresponding issue number?
