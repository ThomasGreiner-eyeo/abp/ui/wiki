# ABP UI development workflow

- [Workflow](#workflow)
- [Dependency update](#dependency-update)
- [Branches](#branches)
- [Removal commits](#removal-commits)
- [Noissue commits](#noissue-commits)

## Workflow

![](res/development-workflow/abp-ui-gitlab-feature-development-workflow.png)

|Branch|Description|
|------|-----------|
|[Default](https://hg.adblockplus.org/adblockplusui)|Mercurial master repository
|[Master](https://gitlab.com/eyeo/adblockplus/adblockplusui/tree/master)|GitLab [eyeo/adblockplus/adblockplusui]() master branch, mirrors *Default*|
|Release#1|Branch for the next release *Release1*|
|Feature#1.1|Feature branch for *Release#1* release|
|Feature#1.2|Feature branch for *Release#1* release|
|Bug fix#1.3|Bug fix branch for *Release#1* release|
|Release#2|Brancht for next release *Release2* after *Release1*|
|Feature#2.1|Feature branch for *Release#2* release|
|Feature#2.2|Feature branch for *Release#2* release|



After each dependency update a new branch for the next dependency update is
created *Release#1*. 

New features are developed in feature branches (*Feature#1.1*, *Feature#1.2*).
Once they are ready they are squash merged into the current Release branch
*Release1*.

After a feature was merged the new Release branch will be merged into the remaning feature branches.

Once all features are ready to be released the commits from the Release branch will be committed to the mercurial master repository *Default* (this will update the mirror *Master* as well) and a [dependeny update](#dependency-update) will be released. A new Release branch *Release2* will be created.

New feature branches will be based off *Release2* (*Release2.1*, *Release2.2*) for the next dependency update.

In case a bug fix for the previous Release branch *Release1* is needed a bug fix branch *Bugfix#1.3* based off *Release1* will be created. Once all bugfixes are ready they will be committed to the mercurial master repository (*Default*) and all new features from the Release branch (*Release2*) will have to be rebased. The result will be merged into the remaining feature branches *feature2.2*.

Additionally we create a *compatibility* branches whenever we need to change something in *adblockplusui* in order to stay compatible with *adblockpluschrome* or *adblockpluscore*. To avoid confusion with regular releases they use a seperate naming schema, see [below](#branches).


## Dependency update

A *dependency update* is a commit that changes the corresponding line in *dependencies* file used by out [buildtools](https://hg.adblockplus.org/buildtools). For ABP UI this most commonly means updating the dependency to [adblockplusui](https://hg.adblockplus.org/adblockplusui) in the [adblockpluschrome](https://hg.adblockplus.org/adblockpluschrome) *dependencies* file.

Each dependency update is typically accompanied by a trac issue detailing the included changes, see [trac#6476](https://issues.adblockplus.org/ticket/6476) for example.

## Branches

- [Major release](#major-release-branches)
- [Minor release](#minor-release-branches)
- [Compatibility release](#compatibility-release-branches)
- [Feature](#feature-branches)
- [Master](#master)

### Major release

These branches contain any feature and fixes relating to those for the next major UI release.

```bnf
"release-" year "-" number
```

For example, `release-2018-1` is the first major release of 2018.

### Minor release

These branches contain any fixes relating to any past release.

```bnf
"release-" year "-" number "." number
```

For example, `release-2018-1.1` is the first minor release based on the `release-2018-1` branch.

### Compatibility release

These branches contain any changes required for staying compatible with adblockpluschrome and/or adblockpluscore.

```bnf
"compat-" branch "-" year "-" number
```

For example, `compat-next-2018-1` is the first compatibility release of 2018 based on adblockpluschrome's `next` branch.

### Feature

These branches contain changes that are still under development related to a given feature for which UI Nightlies should be provided.

```bnf
"feature-" number "-" name
```

For example, `feature-123-add-foo` is the feature branch for issue #123 and has the name "add-foo".

### Master branch

This branch is a mirror of [hg.adblockplus.org/adblockplusui](https://hg.adblockplus.org/adblockplusui/shortlog/default). It contains changes that are ready for inclusion in the extension via a [dependency update](#dependency-update) as well as changes that have no effect on the extension (e.g. changes to the GitLab CI configuration file).

## Removal commits

Dedicated removal commits should be made when removing modules, components or other significant code parts that we may need to refer back to later on (e.g. for reusing parts of a removed component).

Those commits should ideally be done as part of the same issue that makes the removed code redundant. Alternatively, a follow-up issue can be created for the purpose of removing such code.

## Noissue commits

Noissue commits are code changes that don't have an issue associated with them. They lack context and further information for other stakeholders (i.e. integrators, testers, translators) and are not part of the regular development flow (i.e. no Copy LGTM, Legal LGTM, Feature QA, etc.).

Noissue commit messages should include a link to the respective review, if applicable.

Allowed for…
- Importing translations from Crowdin and XTM (review required)
- Adding release tags (no review required)
- Backing out changes (no review required)

Debatable for…
- Adding/updating documentation (excl. licenses)
- Adding (yet) unused code
- Updating dependencies
- Changing mock-related code
- Backporting changes from other branch
- Fixing linting errors

Not allowed for…
- Minor bug fixes
- Changing metadata files (e.g. .gitignore)
- Changing build-related code
- Changing translation files
- Removing unused files/code
- …and everything that's not explicitly allowed
