## **ABP UI Goals 2020**



*   Realize our full revenue potential
    *   Promote donations in different locations in ABP
*   Improve user acquisition and retention of AA users on desktop
    *   Increase amount of store reviews
    *   Analyze why users uninstalling
    *   Provide features to increase user's stickiness
    *   Improve user awareness to features and ABP capabilities 


## **Implementation goals:**



*   Finish Implement[ Milestone 2019-4](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/boards/1367798?milestone_title=Release%202019-4&) **ABP 3.10**
*   Implement[ Milestone 2020-1](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/boards/1781757?milestone_title=Release%202020-1&)
*   Prepare for implementation -[ Milestone 2020-2](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/boards/1519189?milestone_title=Release%202020-1&)


## **Status phases**



1. Analyze
2. Draft
3. Copy LGTM
4. QA LGTM
5. Legal LGTM
6. Implementation
7. Implementation Review
8. Waiting for 3rd party
9. Feature QA
10. Waiting to be translated
11. Translation
12. Waiting to be released


## **Roadmap Items**


<table>
  <tr>
   <td><strong>Item</strong>
   </td>
   <td><strong>Description</strong>
   </td>
   <td><strong>Status</strong>
   </td>
   <td><strong>Goal</strong>
   </td>
   <td><strong>Goal's data</strong>
   </td>
   <td><strong>Milestone</strong>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/589">Data corruption warning - Behavior change</a>
   </td>
   <td>Inform users about data corruption in non-intrusive approach
   </td>
   <td>11. Translation
   </td>
   <td>Reduce uninstalls due to data corruption
   </td>
   <td>
   </td>
   <td>2019-3
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/562">New version page</a>
   </td>
   <td>Inform user by Bubble UI notification about new versions
   </td>
   <td>10. Waiting to be translated
   </td>
   <td>Increase user awareness to ABP features and brand awareness
   </td>
   <td>
   </td>
   <td>2019-3
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/594">Popup notification icon</a>
   </td>
   <td>Make our notifications more visible
   </td>
   <td>12. Waiting to be released
   </td>
   <td>Increase user awareness to ABP features and brand awareness
   </td>
   <td>
   </td>
   <td>2019-3
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/162">Update notification design of taskbar icon</a>
   </td>
   <td>Animate the ABP icon to inform users about notifications
   </td>
   <td>12. Waiting to be released
   </td>
   <td>Improve product-user communication
   </td>
   <td>
   </td>
   <td>2019-3
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/485">Social icons - Total ads blocked</a>
   </td>
   <td>Allow users to share the total amount of blocked Ads
   </td>
   <td>11. Translation
   </td>
   <td>Get more installs by new users
   </td>
   <td>
   </td>
   <td>2019-3
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/472">Report Issue & Block Element Buttons Design</a>
   </td>
   <td>Improve both buttons design
   </td>
   <td>10. Waiting to be translated
   </td>
   <td>Increase user engagement to use those features more often
   </td>
   <td>
<ul>

<li>More users will report more often about issue

<li>More custom filters (When we have Telemetry)
</li>
</ul>
   </td>
   <td>2019-4
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/638">Block Element - New UI/UX</a>
   </td>
   <td>Improve UX of block element feature to make it easier to user
   </td>
   <td>7. Implementation Review (High)
   </td>
   <td>Make the feature more accessible in order to allow users to have better control on their WEB Experience
   </td>
   <td>
<ul>

<li>More custom filters (When we have Telemetry)
</li>
</ul>
   </td>
   <td>2019-4
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/484">Retry failed filter list updates</a>
   </td>
   <td>Refresh more frequently a filter list that had an error
   </td>
   <td>8. Waiting for 3rd party (Medium)
   </td>
   <td>Keep filter lists up to date to make sure ABP perform as should
   </td>
   <td>
<ul>

<li>Less reporting issues related to non-updated filters

<li>Less uninstalls
</li>
</ul>
   </td>
   <td>2019-4
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/9">Language filter list suggestions</a>
   </td>
   <td>Suggest/enable filter lists based on the websites a user is visiting
   </td>
   <td>4. QA LGTM (High)
   </td>
   <td>Provide automatic solution to approximately 5% of our users who visit websites in languages which they didn't install the proper language filter for
   </td>
   <td>
<ul>

<li>More installed filters (When we have Telemetry)

<li>Less reporting issues related to language filters
</li>
</ul>
   </td>
   <td>2020-1
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/issues/513">I don't care about cookies</a>
   </td>
   <td>Inform user by Bubble UI notification about our feature to block cookie messages
   </td>
   <td>6. Implementation
   </td>
   <td>Increase user awareness to ABP features
   </td>
   <td>
   </td>
   <td>2020-1
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/753">Mention in error message that filters in invalid filter lists are disabled</a>
   </td>
   <td>Inform users about HTTP filter lists they  added in the past
   </td>
   <td>4. QA LGTM
   </td>
   <td>Improve adblocking
   </td>
   <td>
<ul>

<li>More active filter lists (Telemetry)
</li>
</ul>
   </td>
   <td>2020-1
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/184">Provide more flexibility with the notification system</a>
   </td>
   <td>We repeatedly run into limitations that prevent us from using the notification system. We want to make it more flexible so we can use it more often
   </td>
   <td>
<ol>

<li>Analyze
</li>
</ol>
   </td>
   <td>Improve feature/brand awareness
   </td>
   <td>
   </td>
   <td>2020-1
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/adblockplusui/issues/4">Opt-in telemetry data collection</a>
   </td>
   <td>Add Opt-in Telemetry
   </td>
   <td>4. QA LGTM
   </td>
   <td>Get to know users behavior better so we can improve the product
   </td>
   <td>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/716">Bubble UI - Teaser footer</a>
   </td>
   <td>Make the Bubble UI dynamic with different messages
   </td>
   <td>
<ol>

<li>Analyze(High)
</li>
</ol>
   </td>
   <td>Create another location for donations and ratings, but also provide place to increase user engagement by sharing relevant articles from help center/blog
   </td>
   <td>
<ul>

<li>More traffic to blog posts and Help center

<li>More donations per day

<li>More ratings per day
</li>
</ul>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/713">First Run Page</a>
   </td>
   <td>Add donations and ratings promo to the FRP
   </td>
   <td>
<ol>

<li>Analyze(Medium)
</li>
</ol>
   </td>
   <td>Increase chance for donations by users
   </td>
   <td>
<ul>

<li>More donations per day

<li>More ratings per day
</li>
</ul>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/769">Donation button in settings page</a>
   </td>
   <td>Add donations promo to the Settings page
   </td>
   <td>
<ol>

<li>Analyze
</li>
</ol>
   </td>
   <td>Increase chance for donations by users
   </td>
   <td>
<ul>

<li>More donations per day
</li>
</ul>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/758">Undo block element</a>
   </td>
   <td>Manage custom filters from the Bubble UI
   </td>
   <td>4. QA LGTM
   </td>
   <td>Make the Block Element more accessible for users to make sure that users won't hesitate to use it
   </td>
   <td>
<ul>

<li>More custom filters (When we have Telemetry)
</li>
</ul>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/781">Whitelisted websites: Sort by domain + Undo deleting</a>
   </td>
   <td>Improve the UX of the Whitelisted websites
   </td>
   <td>
<ol>

<li>Analyze
</li>
</ol>
   </td>
   <td>Improve our Settings page to have a better UI and UX
   </td>
   <td>
   </td>
   <td>2020-2
   </td>
  </tr>
  <tr>
   <td><a href="https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/758">Block Element settings - in bubble UI</a>
   </td>
   <td>Manage blocked elements from the Bubble UI
   </td>
   <td>4. QA LGTM (Medium)
   </td>
   <td>Make our block element feature more accessible and friendly to non-tech users
   </td>
   <td>
<ul>

<li>Higher usage of block element (Telemetry)
</li>
</ul>
   </td>
   <td>2020-2
   </td>
  </tr>
</table>

