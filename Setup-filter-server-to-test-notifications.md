# Setup filter server to test notifications

```
git clone git@gitlab.com:eyeo/adblockplus/abpui/infrastructure.git
cd infrastructure
git checkout notification
cd modules
ln -s private-stub private
cd ..
vagrant up filter1
# should fail, scroll up to see the error msg. should be about hg clone
# if it's about sitescripts it may because of hg.adblockplus.org not available via ipv4 try
# otherwise continue with next error
vagrant ssh filter1
sudo su
hg clone ssh://wspee@hg.code.sf.net/p/sitescripts/code /opt/sitescripts
# exit ssh session and provision again
vagrant provision filter1
# Should fail with
# Error: hg clone --noupdate ssh://wspee@hg.code.sf.net/p/notifications1/code /opt/notifications && chown -R nginx /opt/notifications returned 255 instead of one of [0]
# ssh into machine and setup notifications repo
vagrant ssh filter1
sudo su
hg clone --noupdate ssh://wspee@hg.code.sf.net/p/notifications1/code /opt/notifications && chown -R nginx /opt/notifications
# exit ssh session and provision again
vagrant provision filter1
# should complete without error (warnings are ok)
vagrant ssh filter1
sudo su
# disable ssl and fcgi cache
vim /etc/nginx/sites-enabled/notification.local
/etc/init.d/nginx restart
# Should work now
# Tips:
# Watch server logs
tail -f /var/log/nginx/*
# To update notification repo
vagrant ssh filter1
sudo su
cd /opt/notifications
sudo -u nginx hg pull
```


## Old
Create a notification fork on Bitbucket if you haven't already.

Clone notifications repo

```
local $ hg clone ssh://hg@bitbucket.org/wspee/notifications
```

Add the notification and commit the result

```
local $ hg commit -A
```

Push new notification

```
local $ hg push
```


Now clone the infrastructure repo

```
local $ git clone git@gitlab.com:eyeo/devops/legacy/infrastructure.git
local $ cd infrastructure
```


Apply the following patch if your machine uses python3  0001-Python2.patch

```
local $ patch -p1 < 0001-Python2.patch
```

Create the private folder (see README.md)

```
local $ cd modules
local $ ln -s private-stub private 
local $ cd ..
```

install vagrant and virtual box (see README.md)

```
local $ apt-get install vagrant virtualbox
```


Start the filter server

(may have to execute `vagrant plugin install vagrant-vbguest` to avoid errors with virtualbox shared  folder)

```
local $ vagrant up filter1
```


the provision script may complain that the package `linux-headers-3.16.0-9-amd64` cannot be found. If that's the case ssh into the machine and do a dist-upgrade

```
local $ vagrant ssh filter1
filter1 $ sudo apt-get update
filter1 $ sudo apt-get dist-upgrade
```

Otherwise continue here


SSH into the filter server

```
local $ vagrant ssh filter1
```


Now we need to tweak the nginx configuration to allow unencrypted traffic for debugging:

```
filter1:~$ vim /etc/nginx/sites-available/notification.adblockplus.org
```


We are going to disable the cache, update the server_name and disable ssh as shown below:

```
#fastcgi_cache_path /var/cache/nginx/notification levels=1 keys_zone=notification:1m;

map $arg_lastVersion $group
{
~(?<digit>-.*) $digit;
}

log_format notification '$remote_addr - $remote_user [$time_local] "$request" '
'$status $bytes_sent "$http_referer" '
'"$http_user_agent" "$http_x_forwarded_for" $scheme '
'"$http_accept_language" "$http_host" '
'"$http_x_client_id" '
'$sent_http_abp_notification_version';



server
{
# server_name notification.adblockplus.org;
server_name notification.local


listen 80;
listen [::]:80;

location /
{
rewrite (.*) https://$host$1 permanent;
}
}
server
{
server_name notification.local

listen 80;
listen [::]:80;

# listen 443 ssl http2;
# listen [::]:443 ssl http2;

# ssl_certificate easylist-downloads.adblockplus.org_sslcert.pem;
# ssl_certificate_key easylist-downloads.adblockplus.org_sslcert.key;
# ssl_dhparam /etc/nginx/dhparam.pem;

# add_header Strict-Transport-Security max-age=31536000;


access_log /var/log/nginx/access_log_notification notification;

# redirect server error pages to the static page /50x.html
#
error_page 500 502 503 504 /50x.html;
location = /50x.html
{
root /usr/share/nginx/html;
}

# https://issues.adblockplus.org/ticket/4894
location /easylistchina+easylist.txt
{
if ($http_user_agent ~ "^-?$")
{
return 400;
}
}

if ($http_host ~ "^(.+)\.$")
{
set $canonical_host $1;
rewrite ^(.*) $scheme://$canonical_host$1 permanent;
}






keepalive_timeout 0;

location /.hg
{
internal;
}

location /notification.json
{
fastcgi_pass unix:/tmp/multiplexer-fastcgi.sock;
include /etc/nginx/fastcgi_params;
# fastcgi_cache notification;
# fastcgi_cache_key $group;
# fastcgi_cache_valid any 1m;
# fastcgi_cache_lock on;
}



}

```

Disable `easylist-downloads.adblockplus.org`

```
filter1 $ rm /etc/nginx/sites-enabled/easylist-downloads.adblockplus.org 
```

Now restart nginx
```
filter1 $ sudo /etc/init.d/nginx restart
```


Go to notifications repo and update default url using the ssh url from Bitbucket e.g.: ssh://hg@bitbucket.org/wspee/notifications
```
filter1 $ cd /opt/notifications
```

```
filter1 $ vim .hg/hgrc
```

End result should look like this:
```
filter1 $ cat .hg/hgrc
# example repository config (see 'hg help config' for more info)
[paths]
default = ssh://hg@bitbucket.org/wspee/notifications
```

Now pull the new notification

```
filter1 $ hg pull -u
```

(Notification should now be present)


To download it we need to know the ip of the machine

```
filter1 $ ip addr
...
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
link/ether 08:00:27:c8:56:eb brd ff:ff:ff:ff:ff:ff
inet 10.8.0.120/24 brd 10.8.0.255 scope global eth1
```

Try downloading the notification using the ip of eth1 from the host machine

```
local $ wget 10.8.0.120/notification.json -O -
```

Voila! Now we can use this url iin Adblock Plus to test the notification using a real filter server


