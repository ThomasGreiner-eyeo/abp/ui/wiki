# Release History

| Extension Release | UI Release | UI Revision | Dependency Update Issue |
|-|-|-|-|
| **3.12** | **r20-2** | ------- | [webext#211](eyeo/adblockplus/adblockpluschrome#211) |
| **3.11** | **r20-1** | ------- | [webext#199](eyeo/adblockplus/adblockpluschrome#199) |
| **3.11** | **c20-3** | ------- | [webext#283](eyeo/adblockplus/adblockpluschrome#283) |
| 3.10 | ~~r19-4.1~~ | ------- | [webext#286](eyeo/adblockplus/adblockpluschrome#286) |
| 3.10 | r20-001 | 1f20edc | [webext#268](eyeo/adblockplus/adblockpluschrome#268) |
| 3.10 | c20-2 | 86df3f7 | [webext#270](eyeo/adblockplus/adblockpluschrome#270) |
| 3.10 | r19-4 | 697149d | [webext#143](eyeo/adblockplus/adblockpluschrome#143) |
| 3.9.4 | r19-3.2 | 690850b | [webext#225](eyeo/adblockplus/adblockpluschrome#225) |
| 3.9 | r19-3.1 | a85dfa9 | [webext#191](eyeo/adblockplus/adblockpluschrome#191) |
| 3.9 | r19-3 | 353cf5f | [webext#96](eyeo/adblockplus/adblockpluschrome#96) |
| 3.9 | c20-1 | 4cb8b8c | [webext#153](eyeo/adblockplus/adblockpluschrome#153) |
| 3.8 | r19-2.1 | 82203482 | [webext#122](eyeo/adblockplus/adblockpluschrome#122) |
| 3.8 | r19-2 | 541b886 | [webext#95](eyeo/adblockplus/adblockpluschrome#95) |
| 3.8 | c19-3 | edd3188 | [webext#74](eyeo/adblockplus/adblockpluschrome#74) |
| 3.7 | r19-1.3 | cf49e47 | [webext#91](eyeo/adblockplus/adblockpluschrome#91) |
| 3.7 | r19-1.2 | 8d4d081 | [webext#80](eyeo/adblockplus/adblockpluschrome#80) |
| 3.7 | r19-1.1 | cb2f5cd | [webext#55](eyeo/adblockplus/adblockpluschrome#55) |
| 3.7 | r19-1 | 36ed713 | [webext#55](eyeo/adblockplus/adblockpluschrome#55) |
| 3.6 | r18-5.3 | e4eb28e | [webext#38](eyeo/adblockplus/adblockpluschrome#38) |
| 3.6 | r18-5.2 | 437d5cf | [webext#16](eyeo/adblockplus/adblockpluschrome#16) |
| 3.6 | c19-2 | b9a5480 | [webext#15](eyeo/adblockplus/adblockpluschrome#15) |
| 3.6 | r18-5.1 | 8774527 | trac#6936 |
| 3.6 | r18-5 | bbfb52b | trac#6936 |
| 3.6 | c19-1 | 8aa1f67 | [webext#9](eyeo/adblockplus/adblockpluschrome#9) |
| 3.6 | r18-5.-4 | ec910b0 | trac#7308 |
| 3.6 | r18-5.-5 | 22c1ea9 | trac#7343 |
| 3.5 | r18-5.-6 | 17697d3 | trac#7279 |
| 3.5 | r18-5.-7 | 0a673ad | trac#7249 |
| 0.9.13 (Edge), 3.5 | r18-4.8 | 45db81b | trac#7222 |
| 0.9.13 (Edge), 3.5 | r18-4.7 | d02e690 | trac#7222 |
| 3.4.3 | r18-4.7 | 453a0d9 | trac#7219 |
| 3.4.3 | r18-4.6 | ae3e8f1 | trac#7173 |
| 3.4.3 | --- | efc36f6 | trac#7193 |
| 3.4.3 | --- | 2d63a01 | trac#7193 |
| 3.4.3 | --- | be886f1 | trac#7175 |
| 3.5 | r18-5.-8 | 1660877 | trac#7054 |
| 3.5 | r18-5.-9 | 13f9919 | ----- |
| 3.4.1 | r18-4.5 | 32136d5 | trac#7107 |
| 3.4 | r18-4.4 | d257445 | trac#7069 |
| 3.4 | r18-4.3 | 1106767 | trac#7014 |
| 3.4 | r18-4.2 | 682d4d6 | trac#6962 |
| 3.4 | r18-4.1 | ab57a6b | trac#6892 |
| 3.4 | r18-4 | ------- | ----- |
| 3.4 | r18-3.3 | cb795db | trac#6933 |
| 3.3.1 | r18-3.2.1 | 67288d7 | trac#6900 |
| 3.3 | r18-3.2 | 4cf24d8 | trac#6879 |
| 3.3 | r18-3.1 | e4c4803 | trac#6802 |
| 3.3 | r18-3 | 2fe1f42 | trac#6784 |
| 3.0.3 | r18-2 | 1c660ba | trac#6548 |
| 3.1 | --- | dcb681f | trac#6599 |
| 3.0.3 | --- | 26c795f | trac#6548 |
| 3.0.3 | --- | 7411639 | trac#6511 |
| 3.0.3 | --- | 0290fb1 | trac#6476 |
| 3.0.3 | --- | 93b2850 | trac#6403 |
| 3.0.3 | --- | 8103830 | ---- |
| 3.0.1 | --- | 5bf6edb | trac#6006 |
| 3.0 | --- | f10b15d | trac#5991 |
| 3.0 | --- | cf5ce61 | trac#5982 |
| 3.0 | --- | 915236b | trac#5925 |
| 3.0 | --- | 63e4758 | trac#5880 |
| 3.0 | --- | 4a076b6 | trac#5028 |
| 3.0 | --- | 0fad0fe | trac#5837 |
| 3.0 | --- | b2f10ba | trac#4580 |
| 3.0 | --- | 6defd73 | trac#5593 |
| 3.0 | --- | 311b3d3 | trac#5803 |
| 3.0 | --- | 4958327 | trac#5587, trac#5748 |
| 3.0 | --- | 9460adb | trac#5593 |
| 3.0.3 | --- | e691a7d | ---- |
| --- | --- | 8103830 | trac#6208 |
| 3.0.2 | --- | 643f079 | trac#6178 |
| 3.0.2 | --- | c4e39c7 | trac#6169 |
| 3.0.2 | --- | c762275 | trac#6095 |
| 3.0.2 | --- | 28d1ac1 | trac#6085 |
| 3.0.2 | --- | c703100 | trac#6042 |
| 3.0.2 | --- | 4bc1d98 | trac#6038 |
| 3.0 | --- | 768d723 | trac#5316 |
| --- | --- | e691a7d | trac#3697 |
| 1.13.4 | --- | 1ec5c0d | trac#4978 |
| 1.13.3 | --- | 523b73d | trac#4455, trac#5087, trac#5092 |
| 1.13.3 | --- | 6222fd2 | trac#5023 |
| 1.13 | --- | 1db1e7d | trac#4823 |
| 1.13 | --- | 612cb73 | trac#4136 |
| 1.13 | --- | 2212dd7 | trac#4659 |
| 1.12.4 | --- | 4ce9666 | trac#4295 |
| 1.12 | --- | 9de6da7 | trac#3870 |
| 1.12 | --- | e78579a | trac#3868 |
| 1.12 | --- | 5989747 | trac#3826 |
| 1.12 | --- | 04a1a795 | trac#3829 |
| 1.12 | --- | 60762b3 | trac#3830 |
| 1.12 | --- | 42e195e | trac#3719 |
| 1.12 | --- | 3079852 | trac#3763 |
| 1.11 | --- | b845b0d  | trac#154 |
| 1.10 | --- | 6de95bb | trac#3417 |
| 1.10 | --- | 261b5d3 | trac#2397 |
| 1.9.4 | --- | 4633d20 | trac#3335 |
| 1.9.3 | --- | 50837c1 | trac#3087 |
| 1.8.12 | --- | 7ebacdc | trac#2094 |
| 1.8.12 | --- | 152cae6 | trac#2086 |
| 1.8.12 | --- | c8160f0 | trac#2059 |
| 1.8.11 | --- | af1d3fc | trac#1819 |
| 1.8.10 | --- | b69ec30 | trac#1757 |
| 1.8.10 | --- | fd65af4 | trac#1708 |

## Notes

1. UI release names are shortened (e.g. `r18-2` for `release-2018-2` and `c19-1` for `compat-next-2019-1`).
2. Version numbers in bold refer to releases that are still being worked on.
