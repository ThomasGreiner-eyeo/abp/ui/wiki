We are using latest Web technologies to componentize our UI, and this is a collection of known issues, or special behaviors, related to our components.

### io-list-box

Used as reach, accessible, drop down element with multiple selection, the list-box is currently available bottom-up only, where all items are shown above, and never under it.

Accordingly to its position in the panel, this should never be an issue for our users, but as soon as we want to reuse such component closer to the top of the page, or its scrollable container, we need to address this.
