# UI Release Workflow

1. [Create new release](#create-new-release)
2. [Work on release issues](#work-on-release-issues)
3. [Work on updates page issues](#work-on-updates-page-issues)
4. [Follow translation workflow](translation-workflow) (only for major releases)
5. [Make release](#make-release)
6. [Clean up](#clean-up)

## Create new release

1. Create `release-XXXX-X` branch in [adblockplusui][ui] based on preceding release branch.
2. Create `abp-ui-release-XXXX-X` branch in [spec][spec] based on preceding release branch.
3. Create milestone:
    - Title: `Release XXXX-X`
4. Create board:
    - Title: `Release XXXX-X`
    - Set scope to the above milestone.
    - Add columns for the following labels (in that particular order) via "Add list" menu:
      - ~"State::Analyze (ABPUI)"
      - ~"State::Draft (ABPUI)"
      - ~"State::Copy LGTM (ABPUI)"
      - ~"State::QA LGTM (ABPUI)"
      - ~"State::Legal LGTM (ABPUI)"
      - ~"State::Implementation (ABPUI)"
      - ~"State::Implementation Review (ABPUI)"
      - ~"State::Waiting for 3rd Party (ABPUI)"
      - ~"State::Feature QA (ABPUI)"
      - ~"State::Waiting to be Translated (ABPUI)"
      - ~"State::Translation (ABPUI)"
      - ~"State::Waiting to be released (ABPUI)"
5. Only for major releases: Create [updates page issue](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/new?issuable_template=update-page):
    - Title: `Updates page for Release XXXX-X`
    - Description: Use `update-page` template
6. Create [dependency update issue](https://gitlab.com/eyeo/adblockplus/adblockpluschrome/-/issues):
    - Title: `Update adblockplusui dependency to TBD (release-XXXX-X)`
    - Description:

```md
## Background
This imports the following changes: https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/compare/release-YYYY-Y...release-XXXX-X

See also [Release XXXX-X](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/milestones/XX).

## What to change
Change adblockplusui dependency to `TBD`.

## Hints for testers
See hints for testers in individual issues.

## Missing information
The scope of this UI release hasn't been decided yet.

```

7. Add milestone description:
```
- Dependency update: <link to dependency update issue>
- Updates: <link to updates page issue (only for major releases)>
```

8. Update [release history](releases) accordingly.

## Work on release issues

1. [Follow development workflow](development-workflow).
2. Wait until `Waiting to be released` label is set.

## Work on updates page issues

1. Collect feedback about which issues should make it into updates page.
2. One week before requesting translations:
    1. Finalize which issues should make it into updates page.
    2. Create spec MR for updates page issue for describing changes to the [update-specific parts](#update-specific-changes) of the updates page.
3. Wait until `Waiting to be released` label is set.

### Update-specific changes

The updates page should list mainly changes that are visible to users or fixes for problems that have a significant impact on users. Each major update should include the following changes:

Changes to the updates notification:
- Title
- Message (incl. link for opening the updates page)

Changes to the updates page content:
- Hero image
- Title
- Subtitle
- Improvements (optional)
  - Title
  - Description
  - Documentation link (optional)
  - Image or muted video (optional)
  - Image/video description (only if image or video exists)
- Fixes (optional)
  - Title
  - Description
  - Documentation link (optional)
  - Image or muted video (optional)
  - Image/video description (only if image or video exists)

## Make release

1. Verify all release-related issues are ready (see also [definition of done](definition-done)):
    - Ignore unmerged issues with ~"NotReleaseBlocking (ABPUI)" label.
    - Has ~"State::Waiting to be released (ABPUI)"?
    - References all relevant merge requests under "Related merge requests"?
    - Has "Hints for testers" section, if necessary?
    - Has "Hints for translators" section, if there were changes to translation files?
    - Has "Integration notes" section, if there were changes to a UI fork?
2. Update repository:
    1. Pull master branch.
    2. Checkout and pull release branch (rebase onto master if necessary).
3. Push commits to Mercurial:
    1. Create patches using `git format-patch master`.
    2. Convert git to Mercurial patches using `moz-git-tools/git-patch-to-hg-patch *.patch` (with [patch date fix](https://github.com/ThomasGreiner/moz-git-tools/tree/fixed-hg-patch-date)).
    3. Checkout and pull Mercurial default branch.
    4. Import patches into Mercurial.
    5. Add tag to last commit with the message `Noissue - Added tag XXXX for changeset XXXX` using `hg tag release-XXXX-X` and `hg commit --amend`.
    6. Push commits to remote.
4. Update dependency update issue:
    1. Replace `TBD` in title with commit hash as `hg:XXXX`.
    2. Remove "Missing information" section.
    3. Update "What to change" section with commit hashes (from latest commit in master branch) as `hg:XXXX git:XXXX`.
    4. Notify WebExt team or create dependency update.
5. Update GitLab issues:
    1. Remove `Waiting to be released` label.
    2. Close issue.
6. Update [release history](releases) accordingly.
7. Close milestone.

## Clean up

- Delete release specific board as soon as all release-related issues are closed.
- Notify UI team and rebase release branches in [adblockplusui][ui] and [spec][spec] onto updated master branch.
- Notify UI team and rebase release branches in [adblockpluschrome][webext-fork] and [adblockpluscore][core-fork] onto updated next branch as soon as dependency update has been merged.


[core-fork]: https://gitlab.com/eyeo/adblockplus/abpui/adblockpluscore/
[spec]: https://gitlab.com/eyeo/specs/spec
[ui]: https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/
[webext-fork]: https://gitlab.com/eyeo/adblockplus/abpui/adblockpluschrome/
