# Donation campaigns

We're raising awareness for updates we released and encourage users to donate
to allow us continue working on the extension, by opening a donations/updates
page in a new tab. For that, we're using the [notifications][notifs-repo]
repository to remotely activate/deactivate ["newtab"][notifs-newtab]
notifications.

## Workflow

1. **Preparation**
   1. Define rollout schedule.
   2. Create and review notification changes.  
      _at least one week before rollout_
   3. Test notification changes.  
      _at least two days before rollout_
2. **Rollout**  
   _daily, according to rollout schedule_
   1. Evaluate current status.
   2. Adjust rollout schedule as needed.
   3. Review and push next notification change(s), as agreed on.
   4. Log date and time of push.
   5. Notify stakeholders.

## Notification changes

1. [Create new campaign](#create-new-campaign)
2. [Activate campaign](#activate-campaign)
3. [Increase/decrease campaign rollout](#increase-decrease-campaign-rollout)
4. [Deactivate campaign](#deactivate-campaign)

### Create new campaign

1. Define notification ID as `AdblockplusDonateYYYYMM`.  
  _e.g. "AdblockplusDonate202001"_
2. Create new file whose name is the same as the notification ID.  
  _see [template](#notification-file-template)_
3. Replace `<NOTIFICATION-ID>` placeholders with notification ID.
4. Replace `<MIN-EXTENSION-VERSION>` placeholders with minimum extension version
  for which the notification should be shown.
5. Adjust groups and sample sizes accordingly, based on rollout stages.  
  _Note: The sum of all sample sizes should not exceed `1`._

### Activate campaign

1. Set `inactive` property to `false`.

### Increase/decrease campaign rollout

1. Add/remove the `title` and `message` properties to/from the respective groups
  to adjust its exposure accordingly.

### Deactivate campaign

1. Set `inactive` property to `true`.


### Notification file template

```ini
inactive = true
severity = newtab

title.en-US = <NOTIFICATION-ID>
message.en-US = <NOTIFICATION-ID>
links = https://new.adblockplus.org/update

target = extension=adblockpluschrome extensionVersion>=<MIN-EXTENSION-VERSION> locales=ar,am,bg,bn,ca,cs,da,el,en,en-CA,en-GB,en-US,es,es-419,et,fa,fi,fil,gu,he,hi,hr,hu,id,it,ja,kn,ko,lt,lv,ml,mr,ms,nl,no,pl,pt-BR,pt-PT,ro,ru,sk,sl,sr,sv,sw,ta,te,th,tr,uk,vi,zh-CN,zh-TW
target = extension=adblockplusfirefox extensionVersion>=<MIN-EXTENSION-VERSION> locales=ar,am,bg,bn,ca,cs,da,el,en,en-CA,en-GB,en-US,es,es-419,et,fa,fi,fil,gu,he,hi,hr,hu,id,it,ja,kn,ko,lt,lv,ml,mr,ms,nl,no,pl,pt-BR,pt-PT,ro,ru,sk,sl,sr,sv,sw,ta,te,th,tr,uk,vi,zh-CN,zh-TW

[1]
sample = 0.01

[2]
sample = 0.14

[3]
sample = 0.15

[4]
sample = 0.20

[5]
sample = 0.20

[6]
sample = 0.15

[7]
sample = 0.15
```


[notifs-newtab]: https://gitlab.com/eyeo/specs/spec/-/blob/ae231cbe2f3bbf0bbe3e8dd2b99819406fbf3901/spec/abp/notifications.md#new-tab
[notifs-repo]: https://hg.adblockplus.org/notifications
