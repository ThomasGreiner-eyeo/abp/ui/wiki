# Utilities

- [Asset optimization](#asset-optimization)
- [Font generation](#font-generation)
- [Translation synchronization](#translation-synchronization)

## Asset optimization

Image files in the repository are optimized using either
[gifsicle](https://www.npmjs.com/package/gifsicle),
[pngquant](https://www.npmjs.com/package/pngquant) or
[svgo](https://www.npmjs.com/package/svgo).

### NPM commands

- `optimize.gif <file path>`: Optimize given GIF file.
- `optimize.png <file path>`: Optimize given PNG file.
- `optimize.svg <file path>`: Optimize given SVG file.

## Translation synchronization

### Crowdin

Community translations for the strings in this project are managed using
[Crowdin](https://crowdin.com/project/adblockplusui).

#### Prerequisites

- [Java 7+](https://support.crowdin.com/cli-tool/#requirements)
- [Crowdin CLI tool](https://support.crowdin.com/cli-tool/)

Follow [Crowdin's installation guide](https://support.crowdin.com/cli-tool/#installation)
in order to install its CLI tool.

#### Configuration

You can find our configuration for Crowdin CLI located in the `crowdin.yml` file
and more information in [Crowdin's documentation](https://support.crowdin.com/configuration-file/).

#### Environment variables

- `CROWDIN_API_KEY`: API key string (retrievable by translation managers)

#### NPM commands

- `crowdin.download-translations`: Download translation updates from Crowdin
    and generate fonts (see [Font generation](#font-generation)).
- `crowdin.upload-strings`: Push source strings (en_US) to Crowdin.
- `crowdin.upload-translations`: Push translations to Crowdin.

### CSV export

_Deprecated. Use [XTM utilities](#xtm) instead._

Translations for [core languages](Core-Languages-for-Translation) are
provided by a translation agency which is using CSV files.

#### NPM commands

- `csv-export -- <commit>`: Exports differences between source strings between
  current and given commit.
- `csv-import -- <file path>`: Imports translations from given file.

#### CSV file format

| Type     | Filename     | StringID | Description          | Placeholders                | en_US         | af         | am  | … |
|----------|--------------|----------|----------------------|-----------------------------|---------------|------------|-----|-----|
| Modified | options.json | cancel   | Cancel button label  |                             | Cancel        | Kanselleer | ይቅር | … |
| Added    | options.json | domain   | Domain input example | {"domain":{"content":"$1"}} | e.g. $domain$ |            |     | … |

### XTM

Translations for [core languages](Core-Languages-for-Translation) are
provided by a translation agency and are managed using
[XTM Cloud](https://xtm.cloud).

#### Environment variables

- `CLIENT`: Company ID string
- `PASSWORD`: User password string
- `USER_ID`: Numerical user ID

#### NPM commands

- `xtm.build`: Generate downloadadble files for `xtm.download`.
    Use only when translations need to be downloaded before XTM workflow finishes.
- `xtm.create`: Create a new project and upload source string changes.
- `xtm.download`: Download translations and apply changes to translation files.
- `xtm.update`: Upload and apply source string changes to project.

**Note:** The project name is expected to be name of the current git branch.
